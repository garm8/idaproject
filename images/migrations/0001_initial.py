# Generated by Django 3.2.4 on 2021-06-24 10:08

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Image',
            fields=[
                ('guid', models.UUIDField(default=uuid.UUID('805d141d-695e-47dc-8a8f-01f24125607d'), primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=1024)),
            ],
            options={
                'db_table': 'images',
                'ordering': ['guid'],
            },
        ),
        migrations.CreateModel(
            name='ImageFile',
            fields=[
                ('guid', models.UUIDField(default=uuid.UUID('a3a1afdf-cb7a-47b2-a921-5563e0fdb14d'), primary_key=True, serialize=False)),
                ('file', models.FileField(upload_to='')),
                ('image', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='image_files', to='images.image')),
            ],
            options={
                'db_table': 'image_files',
                'ordering': ['guid'],
            },
        ),
    ]
