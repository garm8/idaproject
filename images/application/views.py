import uuid
from django.core.handlers.wsgi import WSGIRequest
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from images.domain.models import ImageFile
from images.application.services.validators import validate_uploaded_data, validate_edit_params
from images.domain.services.managers import ImageManager
from idaproject.settings import BASE_DIR, MEDIA_URL


def base_page(request: WSGIRequest) -> render:

    if request.method == 'GET':
        images = ImageManager.get_all_images()
        return render(request=request, template_name='main_page.html', context={'images': images})


def upload_page(request: WSGIRequest) -> HttpResponseRedirect:
    if request.method == 'POST':
        uploaded_data = {
            'url': request.POST.get('url', None),
            'file': request.FILES.get('file', None),
        }
        if validate_uploaded_data(uploaded_data):
            ImageManager.upload_image(initial_data=uploaded_data)
            return HttpResponseRedirect(reverse('base_page'))
    else:
        return render(request=request, template_name='upload_page.html')


def view_image(request: WSGIRequest, image_guid: uuid.uuid4) -> render:

    if request.method == 'GET':
        image = ImageManager.get_image(image_guid=image_guid)
        return render(request=request, template_name='view_page.html', context={'image': image})

    if request.method == 'POST':
        if edit_params := validate_edit_params(request.POST):
            image = ImageManager.get_image(image_guid=image_guid)
            ImageManager.edit_image(edit_params=edit_params, image=image)
            image = ImageManager.get_image(image_guid=image_guid)
            return render(request=request, template_name='view_page.html', context={'image': image})


def get_image_file(request: WSGIRequest, image_guid: uuid.uuid4, image_file_guid: uuid.uuid4) -> HttpResponse:

    MEDIA_DIRECTORY = str(BASE_DIR) + MEDIA_URL

    if request.method == 'GET':
        image_file = ImageFile.objects.get(guid=image_file_guid)
        with open(MEDIA_DIRECTORY + str(image_file.file), 'rb') as file:
            response = HttpResponse(content=file.read(), content_type='image/jpg')
            return response
