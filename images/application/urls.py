from django.urls import path
from images.application.views import base_page, upload_page, view_image, get_image_file


urlpatterns = [
    path('', base_page, name='base_page'),
    path('upload_file/', upload_page, name='upload_page'),
    path('<image_guid>/', view_image, name='view_image'),
    path('<image_guid>/image_file/<image_file_guid>/', get_image_file, name='get_image_file'),
]
