def validate_uploaded_data(uploaded_data: dict) -> bool:
    if uploaded_data.get('url') and uploaded_data.get('file'):
        raise Exception('chose the only one way to upload image')
    if not uploaded_data.get('url') and not uploaded_data.get('file'):
        raise Exception('chose one way to upload image')
    return True


def validate_edit_params(init_data: dict) -> dict:
    edit_params = {}
    if init_data.get('width', None):
        width = init_data.get('width', None)
        edit_params['width'] = int(width)
    if init_data.get('height', None):
        height = init_data.get('height', None)
        edit_params['height'] = int(height)
    if edit_params:
        return edit_params
    else:
        raise Exception('zero params')
