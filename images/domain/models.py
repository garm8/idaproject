import uuid
from django.db import models
from django.utils import timezone


class Image(models.Model):
    guid = models.UUIDField(primary_key=True, default=uuid.uuid4())
    name = models.CharField(max_length=1024)
    date = models.DateTimeField(default=timezone.now())

    class Meta:
        db_table = 'images'
        ordering = ['guid', ]


class ImageFile(models.Model):
    guid = models.UUIDField(primary_key=True, default=uuid.uuid4())
    file = models.FileField()
    date = models.DateTimeField(default=timezone.now())
    image = models.ForeignKey(Image, on_delete=models.PROTECT, related_name='image_files')

    class Meta:
        db_table = 'image_files'
        ordering = ['guid', ]
